# -*- coding: utf8-*-
from setuptools import setup
import sys

ver_dic = {}
version_file = open("version.py")
try:
    version_file_contents = version_file.read()
finally:
    version_file.close()

exec(compile(version_file_contents, "version.py", 'exec'), ver_dic)

build_exe_options = {
    "packages": ["os"],
    # "excludes": ["tkinter"],
    "compressed": True
}

base = None
options = None
executables = None

setup(
    name="xnat-connect",
    version=ver_dic["VERSION_TEXT"],
    description="Commandline Tool for accessing XNAT-servers, downloading and uploading data",
    author="Hannes Wahl",
    author_email="hannes.wahl@uniklinikum-dresden.de",
    setup_requires=['setuptools>=18.0', ],
    install_requires=['xnat'],
    python_requires='>3.5.2',
    packages=['xnat_connect'],
    entry_points={
        'console_scripts': ['xnat_connect=xnat_main:main', 'xnat_downloader=xnat_connect.xnat_downloader:main', 'xnat_uploader=xnat_connect.xnat_downloader:main'],
    },
    options=options,
    executables=executables
)