#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl
# https://www.youtube.com/watch?v=VcN94yMOkyU&list=PL8B63F2091D787896&index=10

from PyQt5.Qt import *


class LogTextEdit(QTextEdit):
    def __init__(self):
        super().__init__()

    def onUpdateText(self, text):
        cursor = self.textCursor()
        cursor.movePosition(QTextCursor.End)
        cursor.insertText(text)
        self.setTextCursor(cursor)
        self.ensureCursorVisible()
        self.setReadOnly(True)
        self.setMinimumWidth(400)
