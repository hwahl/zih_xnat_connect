#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl


from PyQt5.QtWidgets import QDialog, QMessageBox, QErrorMessage
# from PyQt5 import QtCore, QtGui
from PyQt5 import uic

from os.path import join
from time import sleep

import xnat


class XNATLogin(QDialog):

    def __init__(self, parent=None):
        super(QDialog, self).__init__(parent)
        uic.loadUi(join("ui", "xnat_login.ui"), self)

        self.uiHost.setText("https://xnat.zih.tu-dresden.de")
        self.uiUser.setFocus()
        self.uiButtonBox.accepted.connect(self.checkCredentials)

    def checkCredentials(self):

        host = self.uiHost.text()
        user = self.uiUser.text()
        pw = self.uiPassword.text()

        # reset empty strings to None
        if user == "":
            user = None
        if pw == "":
            pw = None
        try:
            with xnat.connect(host, user, pw):
                QMessageBox.about(self, "Login Status", "Connection to XNAT-host: {} successful.".format(host))
            self.accept()
        except Exception as e:
            QErrorMessage.about(self, "Login Status",
                                "Connection to XNAT-host: {} failed.\nError Message: {}".format(host, e))
            self.show()
