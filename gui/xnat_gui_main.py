#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl

# from PyQt5.QtWidgets import *
# from PyQt5 import QtCore, QtGui
# from PyQt5.QtGui import *
from PyQt5.Qt import *
from PyQt5 import uic

from os.path import join

import sys

from .xnat_downloader_gui import XNATDownloaderGUI

class Stream(QObject):
    """
    https://stackoverflow.com/questions/44432276/print-out-python-console-output-to-qtextedit
    """
    newText = pyqtSignal(str)

    def write(self, text):
        self.newText.emit(str(text))


class XNATGUIMain(QMainWindow):

    def __init__(self, parent=None):

        super(QWidget, self).__init__(parent)
        uic.loadUi(join("ui", "xnat_gui_main.ui"), self)

        sys.stdout = Stream(newText=self.onUpdateText)

        self.uiCentralWidget = XNATDownloaderGUI()

    def onUpdateText(self, text):
        cursor = self.uiLog.textCursor()
        cursor.movePosition(QTextCursor.End)
        cursor.insertText(text)
        self.uiCentralWidget.uiLog.setTextCursor(cursor)
        self.uiCentralWidget.uiLog.ensureCursorVisible()
        self.uiCentralWidget.uiLog.setReadOnly(True)
        self.uiCentralWidget.uiLog.setMinimumWidth(400)

    def __del__(self):
        sys.stdout = sys.__stdout__
