#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl
# https://www.youtube.com/watch?v=VcN94yMOkyU&list=PL8B63F2091D787896&index=10

# from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui
# from PyQt5.QtGui import *
from PyQt5.Qt import *
from PyQt5 import uic

from os.path import join

import sys


class Node(QTreeWidgetItem):

    def __init__(self, name, parent=None):
        super().__init__(name, parent)
        self._name = name
        self._children = []
        self._parent = parent

        if parent is not None:
            parent.addChild(self)

    def typeInfo(self):
        return "NODE"

    def addChild(self, child):
        self._children.append(child)

    def insertChild(self, position, child):

        if position < 0 or position > len(self._children):
            return False

        self._children.insert(position, child)
        child._parent = self

        return True

    def removeChild(self, position, child):

        if position < 0 or position > len(self._children):
            return False

        child = self._children.pop(position, child)
        child._parent = None

        return True

    def name(self):
        return self._name

    def setName(self, name):
        self._name = name

    def child(self, row):
        return self._children[row]

    def childCount(self):
        return len(self._children)

    def parent(self):
        return self._parent

    def row(self):
        if self._parent is not None:
            return self._parent._children.index(self)

    def log(self, tabLevel=-1):
        output = ""
        tabLevel += 1

        for i in range(tabLevel):
            output += "\t"

        output += "|----{0}\n".format(self._name)

        for child in self._children:
            output += child.log(tabLevel)

        tabLevel -= 1

        # output += "\n"

        return output

    def log_dict(self):

        output = {}
        output["name"] = self._name
        if self._children:

            for child in self._children:
                output[child._name] = child.log_dict()

        return output

    def __repr__(self):
        return self.log()


class ProjectNode(Node):

    def __init__(self, name, parent=None):
        super().__init__(name, parent)
        self.setFlags(self.flags() | Qt.ItemIsTristate | Qt.ItemIsUserCheckable)
        self.setCheckState(0, Qt.Unchecked)

    def typeInfo(self):
        return "Project"


class SubjectNode(Node):

    def __init__(self, name, xnat_id, parent=None):
        super().__init__(name, parent)
        self._xnat_id = xnat_id
        self.setFlags(self.flags() | Qt.ItemIsTristate | Qt.ItemIsUserCheckable)
        self.setCheckState(0, Qt.Unchecked)

    def typeInfo(self):
        return "Subject"


class ExperimentNode(Node):

    def __init__(self, name, xnat_id, parent=None):
        super().__init__(name, parent)
        self._xnat_id = xnat_id
        self.setFlags(self.flags() | Qt.ItemIsUserCheckable)
        self.setCheckState(0, Qt.Unchecked)


    def typeInfo(self):
        return "Session"


class ScanNode(Node):

    def __init__(self, name, parent=None):
        super().__init__(name, parent)

    def typeInfo(self):
        return "Scan"


class ResourceNode(Node):

    def __init__(self, name, parent=None):
        super().__init__(name, parent)

    def typeInfo(self):
        return "Resource"


class XNATDataModel(QAbstractItemModel):
    """INPUTS: Node, QObject"""

    def __init__(self, root, parent=None):
        super().__init__(parent)
        self._rootNode = root

    def rowCount(self, parent):
        """
        :param parent: QModelIndex
        :return: int
        """
        if not parent.isValid():
            parentNode = self._rootNode
        else:
            parentNode = parent.internalPointer()

        return parentNode.childCount()

    def columnCount(self, parent):
        """
        :param parent: QModelIndex
        :return: int
        """
        return 1

    def data(self, index, role):
        """
        :param index: QModelIndex
        :param role:int
        :return: QVariant
        """
        if not index.isValid():
            return None

        node = index.internalPointer()

        if role == Qt.DisplayRole or role == Qt.EditRole:
            if index.column() == 0:
                return node.name()
            else:
                return node.typeInfo()

    def headerData(self, section, orientation, role):
        """
        :param section: int
        :param orientation: Qt.Orientation
        :param role: int
        :return: QVariant
        """
        if role == Qt.DisplayRole:
            if section == 0:
                return "Data"
            else:
                return "TypeInfo"

    def setData(self, index, value, role=Qt.EditRole):
        """
        :param index: QModelIndex
        :param value: QVariant
        :param role: int (flag)
        :return:
        """
        if index.isValid():
            if role == Qt.EditRole:
                node = index.internalPointer()
                node.setName(value)

                return True

        return False

    def flags(self, index):
        """
        :param index: QModelIndex
        :return: int (flag)
        """
        return Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsEditable

    """INPUTS: QModelIndex"""
    """OUTPUTS: QModelIndex"""
    """Should return the parent of the node with the given QModelIndex"""

    def parent(self, index):
        node = self.getNode(index)
        parentNode = node.parent()

        if parentNode == self._rootNode:
            return QModelIndex()

        return self.createIndex(parentNode.row(), 0, parentNode)

    """INPUTS: int, int, QModelIndex"""
    """OUTPUT: QModelIndex"""
    """Should return an QModelIndex that corresponds to the given row, column and parent node"""

    def index(self, row, column, parent):

        parentNode = self.getNode(parent)

        childItem = parentNode.child(row)

        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QModelIndex()

    def getNode(self, index):
        """
        :param index: QModelIndex
        :return:
        """

        if index.isValid():
            node = index.internalPointer()
            if node:
                return node

        return self._rootNode

    def insertRows(self, position, rows, parent=QModelIndex(), nodeType=""):

        nodeTypes = {"default": Node, "project": ProjectNode, "subject": SubjectNode, "session": ExperimentNode,
                     "resource": ResourceNode}

        try:
            selectedNode = nodeTypes[nodeType.lower()]
        except:
            selectedNode = nodeTypes["default"]

        parentNode = self.getNode(parent)
        self.beginInsertRows(parent, position, position + rows - 1)
        for i in range(rows):
            childCount = parentNode.childCount()
            childNode = selectedNode("untitled{}".format(childCount))

            success = parentNode.insertChild(position, childNode)

        self.endInsertRows()

        return success

    def removeRows(self, position, rows, parent=QModelIndex()):

        parentNode = self.getNode(parent)
        self.beginRemoveRows(parent, position, position + rows - 1)

        for i in range(rows):
            success = parentNode.removeChild(position)

        self.endRemoveRows()
        return success


class XNATDownloaderGUI(QWidget):

    def __init__(self, xnat_connector=None, parent=None):

        super(QWidget, self).__init__(parent)
        uic.loadUi(join("ui", "xnat_downloader.ui"), self)

        self._model = None
        self._proxyModel = None

        self.project_nodes = {}
        self.subject_nodes = {}
        self.experiment_nodes = {}

        self.xnat_connector = xnat_connector

        if self.xnat_connector:
            self.setupModel()

        # if self.xnat_connector is None:
        #     P1 = ProjectNode("606", rootNode)
        #     P2 = ProjectNode("1402", rootNode)
        #
        #     for pro in [P1, P2]:
        #         for i in range(0, 10):
        #             sub = SubjectNode('{sub:02d}'.format(sub=i, pro=pro.name()), pro)
        #             for j in range(0, 3):
        #                 ses = SessionNode(
        #                     'pro-{pro}_sub-{sub}_ses-{ses:02d}'.format(sub=sub.name(), pro=pro.name(), ses=j), sub)
        #                 for k in ["T1w", "T2w", "FLAIR"]:
        #                     scan = ScanNode('{}'.format(k), ses)
        #                     for l in ["DICOM", "NIFTI"]:
        #                         res = ResourceNode("{}".format(l), scan)

    def setupModel(self):
        rootNode = Node("ZIH_XNAT")
        with self.xnat_connector.connect():
            self.xnat_connector.get_all_project_ids()
            self.project_nodes = {}
            self.subject_nodes = {}
            self.experiment_nodes = {}
            for pid in self.xnat_connector.project_ids:
                self.project_nodes[pid] = ProjectNode(pid, rootNode)

            selected_pid, button_pressed = QInputDialog.getItem(self, "Select Project-ID", "available IDs:",
                                                                sorted(self.xnat_connector.project_ids))
            self.xnat_connector.get_all_subject_ids(selected_pid)

            for sid, slabel in self.xnat_connector.subject_ids[selected_pid]:
                self.xnat_connector.get_all_experiment_ids(selected_pid, sid)
                self.subject_nodes[sid] = SubjectNode(slabel, sid, self.project_nodes[selected_pid])
                for eid, elabel in self.xnat_connector.experiment_ids[selected_pid][sid]:
                    self.experiment_nodes[eid] = ExperimentNode(elabel, eid, self.subject_nodes[sid])

        self._proxyModel = QtCore.QSortFilterProxyModel()

        self._model = XNATDataModel(rootNode)
        columnTitles = ["Project", "Subject", "Experiment"]
        for i in range(len(columnTitles)):
            self._model.setHeaderData(i, Qt.Horizontal, columnTitles[i], Qt.DisplayRole)

        self._proxyModel.setSourceModel(self._model)
        self._proxyModel.setDynamicSortFilter(True)
        self._proxyModel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)

        self.uiTree.setModel(self._proxyModel)
        # self.uiColumn.setModel(self._proxyModel)

    def setXNATConnector(self, xnat_connector):

        self.xnat_connector = xnat_connector

        if self.xnat_connector:
            self.setupModel()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setStyle("Windows")

    wnd = XNATDownloaderGUI()
    wnd.show()

    sys.exit(app.exec_())
