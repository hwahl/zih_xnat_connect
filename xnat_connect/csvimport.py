#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl
"""
based on information described here: https://xnat.vanderbilt.edu/index.php/Upload_data_to_XNAT
"""

""" Example
object_type,project_id,subject_label,session_type,session_label,as_label,as_type,as_description,quality,resource,fpath
scan,VUSTP,VUSTP11,MR,VUSTP11a,301,T1,T1/3D/TW,questionable,NIFTI,/home/upload/t1.nii.gz
scan,VUSTP,VUSTP11,MR,VUSTP11a,401,DTI,diffusion weighted image,questionable,DICOM,/home/upload/dti.dcm
scan,VUSTP,VUSTP11,MR,VUSTP11a,401,DTI,diffusion weighted image,questionable,NIFTI,/home/upload/dti.nii.gz

{
    "object_type": scan,
    "project_id": VUSTP,
    "subject_label": VUSTP11,
    "session_type": MR,
    "session_label": VUSTP11a,
    "as_label", "
}

"""

header = ["object_type", "project_id", "subject_label", "session_type", "session_label", "as_label", "as_type", "as_description", "quality", "resource", "fpath"]
