#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl
"""
Module for general argparsing, to avoid errors in different modules
"""

import datetime

from argparse import ArgumentParser, ArgumentTypeError


def valid_date(s):
    """
    source: https://stackoverflow.com/a/25470943
    check if date is valid, for argparse
    :param s: date in YYYY-MM-DD formt
    :return:
    """
    try:
        return datetime.datetime.strptime(s, "%Y-%m-%d").date()
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise ArgumentTypeError(msg)

class DefaultArgumentParser(ArgumentParser):

    def __init__(self, description=None):
        super().__init__(description=description)

        self.add_argument('--xnathost', required=True, help='xnat hostname')
        self.add_argument('--xnatuser', default=None, required=False, help='xnat user')
        self.add_argument('--xnatpw', default=None, required=False, help='xnat password')
        self.add_argument('--project', nargs="+", required=True, help='Project id(s), can also be a whitespace separated list')
        self.add_argument('--subject', nargs="+", default='.*',
                          help='subject, regex for filtering subjects, can also be a whitespace separated list with expressions/names, default: ".*"')
        self.add_argument('--session', nargs="+", default='.*',
                          help='session, regex for filtering sessions, can also be a whitespace separated list with expressions/names, default: ".*"')
        self.add_argument('--scan', nargs="+", default='.*',
                          help='scans, uses "Series Description" field in DICOM data regex for filtering scans, can also be a whitespace separated list with expressions/names, default: ".*"')
        self.add_argument('--resources', nargs="+", default=["DICOM"],
                          help="specify resource type to download, eg. NIFTI DICOM PDF")
        self.add_argument('-l', '--logfile', default=None,
                          help="specify path to logfile, if needed")

    def add_main_args(self):
        type = self.add_mutually_exclusive_group(required=True)
        type.add_argument('-d', '--downloader', action="store_true", help="Select downloader functionality")
        type.add_argument('-u', '--uploader', action="store_true",
                          help="Select uploader functionality -- currently under development")

    def add_downloader_args(self):
        self.add_argument('-s', '--startdate', type=valid_date,
                          help="[only downloader] search sessions from start date, if end date is given, search in that range  - format YYYY-MM-DD")
        self.add_argument('-e', '--enddate', type=valid_date,
                          help="[only downloader] search sessions to end date, if start date is given, search in that range - format YYYY-MM-DD")
        self.add_argument('--download_path', default="/tmp",
                          help='target path for download')
