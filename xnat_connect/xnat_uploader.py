#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl
"""
Upload von DICOM-Daten aus XNAT mit Bibliothek "xnat"

Möglichkeit zu filtern nach Subject_label, Session_label und Series Description

Login entweder alle notwendigen Parameter übergeben oder ~/.netrc anlegen (https://xnat.readthedocs.io/en/latest/static/tutorial.html)
"""

from tempfile import SpooledTemporaryFile
import zipfile

from .xnat_connector import XNATConnector


class XNATUploader(XNATConnector):

    def __init__(self, project_id, xnathost, xnatuser=None, xnatpw=None, re_subject=".*", re_session=".*", re_scan=".*", logfile=None):
        """
        Currently only downloads DICOM-data
        :param project_id: Project-ID like listed in XNAT
        :param xnathost: hostadress of xnat-instance
        :param xnatuser: username, not necessary if ~/.netrc is available
        :param xnatpw: password, not necessary if ~/.netrc is available
        :param re_subject: regex for filtering subjects, can also be a list with expressions/names, default: ".*"
        :param re_session: regex for filtering sessions, can also be a list with expressions/names, default: ".*"
        :param re_scan: regex for filtering scans, can also be a list with expressions/names, default: ".*"
        """
        super().__init__(xnathost=xnathost, project_id=project_id, xnatuser=xnatuser, xnatpw=xnatpw, re_subject=re_subject, re_session=re_session,
                         re_scan=re_scan, logfile=logfile)

    def __call__(self):
        self.run()

    def run(self):
        pass

    def upload_single_file(self, filepath, uri, xnatfilename,  query={}):
        """
        upload single file to resource
        :param filepath:
        :return:
        """

        with open(filepath, "rb") as f:
            self.session.upload("{uri}{xnatfilename}".format(uri=uri if uri.endswith("/") else uri + "/", xnatfilename=xnatfilename), f,
                             query= query)

    def upload_files(self, uri, files, query={}):
        """
        upload multiple files to same resource as a zip-archive, can also be used for single files
        :param str uri: URI to where the files should be uploaded too
        :param dict files: Dictionary with "archivename": "filename"
        :return:
        """
        # add additional query options to query
        query = {**query, **{"extract": "true"}}

        with SpooledTemporaryFile() as tmp:
            with zipfile.ZipFile(tmp, 'w', zipfile.ZIP_DEFLATED) as archive:
                for archivename in files:
                    archive.write(files[archivename], archivename)

            # Reset file pointer
            tmp.seek(0)

            # Write file data to response, use generic 'upload.zip', as this will be automatically extracted and deleted
            self.session.upload("{}upload.zip".format(uri if uri.endswith("/") else uri + "/"), tmp,
                                    query=query, overwrite=True)
            # self.session.put("{}upload.zip".format(uri if uri.endswith("/") else uri + "/"), files={"file": tmp},
            #                     query=query)

            # self.session.put("/REST/projects/5000/subjects/666/experiments/2/scans/4/resources/NIFTI/files?extract=true&format=NIFTI&content=BIDS-compliant",
            #                  files={"data.zip": tmp})

    def update_single_file(self, uri, filepath, xnat_filename):
        """
        update single existing file in XNAT
        :param subject_label:
        :param experiment_label:
        :param scan_id: id for associated scan
        :param resource:
        :param filepath:
        :param xnat_filename:
        :return:
        """
        with open(filepath, "rb") as f:
            self.session.post(uri,
                              query={"overwrite": "true", "inbody": "true"},
                              data={xnat_filename: f})


def main():
    from .xnat_argparse import DefaultArgumentParser

    parser = DefaultArgumentParser(description="Prints all files from a certain scan.")
    args = parser.parse_args()

    uploader = XNATUploader(xnathost=args.xnathost, project_id=args.project, xnatuser=args.xnatuser, xnatpw=args.xnatpw,
                            re_subject=args.subject, re_session=args.session, re_scan=args.scan, logfile=args.logfile)

    with uploader.connect() as uploader.session:
        # if 'resources/NIFTI/' is not specified, files will be uploaded to 'resources/DICOM/'
        uri = "/data/projects/{project_id}/subjects/{subject_label}/experiments/{exp_label}/scans/{scan_id}/resources/NIFTI/files/".format(
            project_id=uploader.project_id, subject_label="666", exp_label="2", scan_id="1")
        context = "Upload files"
        with uploader.timeit_context(context):
            uploader.upload_files(uri=uri,
                              # files={"TestXnatupload.nii.gz": "/tmp/AAHead_Scout_1.nii.gz",
                              #        "TestXnatupload.json": "/tmp/AAHead_Scout_1.json"},
                              files={"test.nii.gz": "/tmp/AAHead_Scout_1.nii.gz"},
                              query={"format": "NIFTI", "content": "BIDS-compliant"})


#
# uploader.session.upload("/data/projects/5000/subjects/666/experiments/2/scans/4/resources/NIFTI/files/test.nii.gz", "/tmp/AAHead_Scout_1.nii.gz",
#                query={"format": "blubb"}, overwrite=True)
#
#
# with open("/tmp/AAHead_Scout_1.nii.gz", "rb") as f:
#     uploader.session.put("/REST/experiments/UKD_XNAT_E00002/scans/4/resources/NIFTI/files/",
#                 query={"format": "blubb", "overwrite": "true", },
#                 files={"ahoi.nii": f})


if __name__ == '__main__':
    main()
