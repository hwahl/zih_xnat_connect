#! env python
"""
Superklasse für Erstellung von Tools die mit XNAT-Kommunizieren müssen
"""
import xnat
import time
import re
import datetime
import logging
from contextlib import contextmanager


class XNATConnector(object):

    def __init__(self, xnathost=None, project_id=None, xnatuser=None, xnatpw=None, re_subject=".*", re_session=".*",
                 re_scan=".*", startdate=None, enddate=None, logfile=None):
        """
        Connector-super-class for downloader and uploader
        :param xnathost: hostadress of xnat-instance
        :param xnatuser: username, not necessary if ~/.netrc is available
        :param xnatpw: password, not necessary if ~/.netrc is available
        :param re_subject: regex for filtering subjects, can also be a list with expressions/names, default: ".*"
        :param re_session: regex for filtering sessions, can also be a list with expressions/names, default: ".*"
        :param re_scan: regex for filtering scans, can also be a list with expressions/names, default: ".*"
        :param startdate: datetime.date-object, starttime for searching in downloader, default: datetime.date(datetime.MINYEAR, 1, 1)
        :param enddate: datetime.date-object, enddate for searching in downloader, default: datetime.date(datetime.MAXYEAR, 12, 31)
        :param logifle: path to logfile, if not set, output will only be send to console/stdout
        """

        self.xnathost = xnathost
        self.xnatuser = xnatuser
        self.xnatpw = xnatpw

        self.re_subject = re.compile(re_subject if not isinstance(re_subject, list) else "|".join(re_subject))
        self.re_session = re.compile(re_session if not isinstance(re_session, list) else "|".join(re_session))
        self.re_scan = re.compile(re_scan if not isinstance(re_scan, list) else "|".join(re_scan))

        if isinstance(startdate, datetime.date):
            self.startdate = startdate
        else:
            self.startdate = datetime.date(datetime.MINYEAR, 1, 1)

        if isinstance(enddate, datetime.date):
            self.enddate = enddate
        else:
            self.enddate = datetime.date(datetime.MAXYEAR, 12, 31)

        # check if startdate is greater than enddate, if so, switch dates
        if (self.enddate - self.startdate) < datetime.timedelta(0):
            print("!!! Startdate is greater than enddate !!!\nstart: {0} vs end: {1}".format(self.startdate,
                                                                                             self.enddate))
            tmp = self.enddate
            self.enddate = self.startdate
            self.startdate = tmp
            print("+++ Corrected +++\nstart: {0} vs end: {1}".format(self.startdate,
                                                                     self.enddate))

        self.project_ids = []
        try:
            if isinstance(project_id, list):
                self.project_ids = sorted(project_id)
            else:
                self.project_ids.append(project_id)
        except:
            raise (IOError, 'project_id {} is not valid'.format(project_id))

        self.subject_ids = {}
        self.experiment_ids = {}
        self.available_resources = []

        # current selected session
        self.session = None
        # current selected project
        self.project = None

        # configure logging
        logging_format = '%(levelname)s: %(asctime)s: %(message)s'
        logging.basicConfig(level=logging.INFO, format=logging_format)
        self.logger = logging.getLogger()
        print('Logfile: {}'.format(logfile))
        # create a file handler
        if logfile:
            handler = logging.FileHandler(logfile)
            # create a logging format
            formatter = logging.Formatter(logging_format)
            handler.setFormatter(formatter)
            # add the handlers to the logger
            self.logger.addHandler(handler)

    def __repr__(self):
        return '{}(host: {)'.format(self.__class__.__name__, self.xnathost)

    def log(self, message, level="info"):

        levels = {"info": self.logger.info, "debug": self.logger.debug, "warning": self.logger.warning,
                  "error": self.logger.error, "critical": self.logger.critical}
        log = levels[level.lower()]
        log(message)

    def connect(self):
        self.session = xnat.connect(self.xnathost, self.xnatuser, self.xnatpw)
        return self.session

    def disconnect(self):
        if self.session:
            self.session.disconnect()
            self.session = None

    def __del__(self):
        if self.session:
            self.session.disconnect()

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.session:
            self.session.disconnect()

    def get_all_project_ids(self):
        """
        get available project ids
        """
        for pid in self.session.projects.keys():
            self.project_ids.append(pid)

        self.project_ids = list(set(self.project_ids))

    def get_all_subject_ids(self, project_id):
        """
        get subject ids with specific pattern

        """
        print("Project-ID: {} # Collect available subjects...".format(project_id))
        self.subject_ids[project_id] = []
        self.experiment_ids[project_id] = {}
        for subject_id in self.session.projects[project_id].subjects.keys():
            self.experiment_ids[project_id][subject_id] = []
            subject_label = self.session.projects[project_id].subjects[subject_id].label
            # filter for matching subject label
            if self.re_subject.match(subject_label):
                self.subject_ids[project_id].append((subject_id, subject_label))
        print("Project-ID: {} # Finished collecting available subjects.".format(project_id))
        # return self.subject_ids

    def get_all_experiment_ids(self, project_id, subject_id):
        """
        get subject ids with specific pattern
        :return: list with tuples, containing (xnat_id, subject_label)
        """
        print("Project-ID: {} # Collect available experiments for subject {}...".format(project_id, subject_id))
        for experiment_id in self.session.projects[project_id].subjects[subject_id].experiments.keys():
            experiment_label = self.session.projects[project_id].subjects[subject_id].experiments[experiment_id].label
            if self.re_session.match(experiment_label):
                self.experiment_ids[project_id][subject_id].append((experiment_id, experiment_label))

    @contextmanager
    def timeit_context(self, context_name):
        startTime = time.time()
        yield
        elapsedTime = time.time() - startTime
        print("[{0}] finished in {1} s".format(context_name, int(elapsedTime)))
