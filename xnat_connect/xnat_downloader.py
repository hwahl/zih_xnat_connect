#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl
"""
Download von DICOM-Daten aus XNAT mit Bibliothek "xnat"

Möglichkeit zu filtern nach Subject_label, Session_label und Series Description

Login entweder alle notwendigen Parameter übergeben oder ~/.netrc anlegen (https://xnat.readthedocs.io/en/latest/static/tutorial.html)
"""

import os

from xnat_connect.xnat_connector import XNATConnector
from xnat_connect.xnat_argparse import DefaultArgumentParser


class UnknownResourceTypeError(ValueError):
    pass


class XNATDownloader(XNATConnector):

    def __init__(self, xnathost=None, project_id=None, xnatuser=None, xnatpw=None, re_subject=".*", re_session=".*",
                 re_scan=".*", resources=None, download_path="./", startdate=None, enddate=None, logfile=None):
        """
        Currently only downloads DICOM-data
        :param project_id: Project-ID like listed in XNAT
        :param xnathost: hostadress of xnat-instance
        :param xnatuser: username, not necessary if ~/.netrc is available
        :param xnatpw: password, not necessary if ~/.netrc is available
        :param re_subject: regex for filtering subjects, can also be a list with expressions/names, default: ".*"
        :param re_session: regex for filtering sessions, can also be a list with expressions/names, default: ".*"
        :param re_scan: regex for filtering scans, can also be a list with expressions/names, default: ".*"
        :param download_path: download path for data
        """
        super().__init__(xnathost=xnathost, project_id=project_id, xnatuser=xnatuser, xnatpw=xnatpw,
                         re_subject=re_subject, re_session=re_session, re_scan=re_scan, startdate=startdate,
                         enddate=enddate, logfile=logfile)

        self.resources = resources
        self.download_path = download_path

    def __call__(self):
        self.run()

    def run(self):
        """
        run main function to obtain and download DICOM scan data
        :return:
        """
        self.log("Try to establish connection to XNAT-host: {}".format(self.xnathost))
        with self.connect() as connection:
            self.log("Connection to XNAT-host: {} successful.".format(self.xnathost))
            self.log("Checking in time frame: {} -> {}.".format(self.startdate, self.enddate))
            self.iterate_projects(connection)
        self.log("Connection to XNAT-host: {} terminated.".format(self.xnathost))

    def iterate_projects(self, connection):
        """
        iterate over given projects
        :return:
        """
        for current_project_id in self.project_ids:
            self.project = connection.projects[current_project_id]
            self.get_subject_ids()

            for subject_id, subject_label in self.subject_ids[self.project.id]:
                subject_path = os.path.abspath(os.path.join(self.download_path, self.project.id, subject_label))
                try:
                    os.makedirs(subject_path)
                except:
                    pass
                subject = self.project.subjects[subject_id]
                self.log("Downloading scans for subject {}...".format(subject_label))
                self.download_scans(subject, subject_path)
                self.log("Finished downloading scans for subject {}.".format(subject_label))

    def get_subject_ids(self):
        """
        get subject ids with specific pattern

        """
        self.log("Project-ID: {} # Collect available subjects...".format(self.project.id))

        self.subject_ids[self.project.id] = []

        for subject_id in self.project.subjects.keys():
            subject_label = self.project.subjects[subject_id].label
            # filter for matching subject label
            if self.re_subject.match(subject_label):
                self.subject_ids[self.project.id].append((subject_id, subject_label))
        self.log("Project-ID: {} # Finished collecting available subjects.".format(self.project.id))
        # return self.subject_ids

    def download_scans(self, subject, path):
        """
        download all scans from subjects, possible filtering for session-name and series description of scan,
        since type is the same as series description so far
        :param subject: XNAT Subject Object
        :param path: absolute path to download directory
        :param re_session: regex for session/experiment label
        :param re_scan: regex for scan name/Series Description
        :return:
        """

        session_ids = subject.experiments.keys()
        # filter for matching session labels
        for session_id in [session_id for session_id in session_ids
                           if self.re_session.match(subject.experiments[session_id].label)]:
            experiment = subject.experiments[session_id]
            # check if experiment-date is between given start- and end-date
            if self.startdate <= experiment.date <= self.enddate:
                # filter for matching series descriptions
                for scan in [scan for scan in experiment.scans.values()
                             if self.re_scan.match(scan.data["series_description"])]:
                    try:
                        if self.resources is None:
                            context = "Subject: {}, Session: {} ,Scan: {}".format(subject.label, experiment.label,
                                                                                  scan.data["series_description"])
                            with self.timeit_context(context):
                                scan.download_dir(path)
                        else:
                            # filter for only valid, existing resources in this scan
                            valid_resources = [res.upper() for res in self.resources if res.upper() in scan.resources]
                            if valid_resources:
                                for res in valid_resources:
                                    resource = scan.resources[res]
                                    context = "Subject: {}, Session: {} ,Scan: {}, Resource: {}".format(subject.label,
                                                                                                        experiment.label,
                                                                                                        scan.data[
                                                                                                            "series_description"],
                                                                                                        res)
                                    with self.timeit_context(context):
                                        resource.download_dir(path)
                            else:
                                # get existing resource labels, has to be this way, because of the Catalog-format of the resources which allows indexing for ID and label as well
                                possible_resource_labels = [scan.resources[x].label for x in scan.resources]
                                possible_resource_ids = [scan.resources[x].id for x in scan.resources]
                                raise UnknownResourceTypeError(
                                    "resources could not be found in scan. possible values: \nlabels: {}, ids: {}".format(
                                        possible_resource_labels,
                                        possible_resource_ids))
                    except Exception as err:
                        print(err)


def main():
    parser = DefaultArgumentParser(description="Downloads all files given project(s), according to set filters.")
    parser.add_downloader_args()
    args = parser.parse_args()

    downloader = XNATDownloader(xnathost=args.xnathost, project_id=args.project, xnatuser=args.xnatuser,
                                xnatpw=args.xnatpw,
                                re_subject=args.subject, re_session=args.session, re_scan=args.scan,
                                resources=args.resources, download_path=args.download_path, startdate=args.startdate,
                                enddate=args.enddate, logfile=args.logfile)

    downloader()


if __name__ == '__main__':
    main()
