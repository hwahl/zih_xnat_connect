#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl
# https://www.youtube.com/watch?v=VcN94yMOkyU&list=PL8B63F2091D787896&index=10

from PyQt5.QtWidgets import QDialog
from PyQt5.Qt import QApplication

from gui.xnat_login import XNATLogin
from gui.xnat_gui_main import XNATGUIMain

import sys

if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setStyle("Fusion")


    main = XNATGUIMain()
    main.show()

    login = XNATLogin()
    login.show()

    if login.exec_() == QDialog.Accepted:
        from xnat_connect.xnat_connector import XNATConnector
        host = login.uiHost.text()
        user = login.uiUser.text() if login.uiUser.text() != "" else None
        pw = login.uiPassword.text() if login.uiPassword.text() != "" else None

        main.uiCentralWidget.setXNATConnector(xnat_connector=XNATConnector(xnathost=host, xnatuser=user, xnatpw=pw))

    sys.exit(app.exec_())
