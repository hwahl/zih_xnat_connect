#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl

"""
Main to invoke downloader, uploader, etc...
"""

from .xnat_connect.xnat_argparse import DefaultArgumentParser

def run_uploader(args):
    from xnat_connect.xnat_uploader import XNATUploader

    uploader = XNATUploader(xnathost=args.xnathost, project_id=args.project, xnatuser=args.xnatuser, xnatpw=args.xnatpw,
                            re_subject=args.subject, re_session=args.session, re_scan=args.scan, logfile=args.logfile)

    with uploader.connect() as uploader.session:
        # if 'resources/NIFTI/' is not specified, files will be uploaded to 'resources/DICOM/'
        uri = "/data/projects/{project_id}/subjects/{subject_label}/experiments/{exp_label}/scans/{scan_id}/resources/NIFTI/files/".format(
            project_id=uploader.project_id, subject_label="666", exp_label="2", scan_id="1")
        context = "Upload files"
        with uploader.timeit_context(context):
            uploader.upload_files(uri=uri,
                                  # files={"TestXnatupload.nii.gz": "/tmp/AAHead_Scout_1.nii.gz",
                                  #        "TestXnatupload.json": "/tmp/AAHead_Scout_1.json"},
                                  files={"test.nii.gz": "/tmp/AAHead_Scout_1.nii.gz"},
                                  query={"format": "NIFTI", "content": "BIDS-compliant"})


def run_downloader(args):
    from xnat_connect.xnat_downloader import XNATDownloader
    downloader = XNATDownloader(xnathost=args.xnathost, project_id=args.project, xnatuser=args.xnatuser,
                                xnatpw=args.xnatpw,
                                re_subject=args.subject, re_session=args.session, re_scan=args.scan,
                                resources=args.resources, download_path=args.download_path, startdate=args.startdate,
                                enddate=args.enddate, logfile=args.logfile)
    downloader()


def main():

    parser = DefaultArgumentParser(description="Prints all files from a certain scan.")
    parser.add_downloader_args()
    parser.add_main_args()
    args = parser.parse_args()

    if args.downloader:
        run_downloader(args)
    if args.uploader:
        run_uploader(args)


if __name__ == "__main__":
    main()
